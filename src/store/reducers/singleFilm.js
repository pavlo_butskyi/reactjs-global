import {
  FETCH_FILM_INFO_REQUEST,
  FETCH_FILM_INFO_SUCCESS,
  FETCH_FILM_INFO_FAILURE,
  CLEAR_FILM_INFO,
} from 'constants/';

const initialState = {
  isError: false,
  isFetching: false,
};

export const singleFilm = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_FILM_INFO_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case FETCH_FILM_INFO_SUCCESS:
      return {
        ...state,
        ...action.singleFilm,
        isError: false,
        isFetching: false,
      };
    case FETCH_FILM_INFO_FAILURE:
      return {
        // clear films data from store
        isError: true,
        isFetching: false,
      };
    case CLEAR_FILM_INFO:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};