import {
  FETCH_FILMS_REQUEST,
  FETCH_FILMS_SUCCESS,
  FETCH_FILMS_FAILURE,
  CLEAR_FILMS,
} from 'constants/';

const initialState = {
  films: [],
  isFetching: false,
  isError: false,
};

export const filmList = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_FILMS_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case FETCH_FILMS_SUCCESS:
      return {
        ...state,
        films: action.films,
        isError: false,
        isFetching: false,
      };
    case FETCH_FILMS_FAILURE:
      return {
        ...state,
        isError: true,
        isFetching: false,
      };
    case CLEAR_FILMS:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};