import {
  SET_SEARCH,
  CLEAR_SEARCH,
} from 'constants/';

const initialState = {
  search: '',
  searchBy: 'title', // 'genres'
  sortBy: 'release_date', // 'vote_average'
  limit: 50,
  sortOrder: 'desc', // 'asc'
};

export const searchData = (state = initialState, action) => {
  switch (action.type) {
    case SET_SEARCH:
      return {
        ...state,
        ...action.searchData,
      };
    case CLEAR_SEARCH:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};