import {
  createStore,
  applyMiddleware,
  compose,
} from 'redux';

import {
  createLogger,
} from 'redux-logger';

import {
  rootReducer,
  rootSaga,
} from './root-reducer';

import createSagaMiddleware, {END} from 'redux-saga';

const sagaMiddleware = createSagaMiddleware();

const middlewares = [sagaMiddleware];

let composeEnhancers = compose;

if (process.env.NODE_ENV === 'development') {
  if (typeof window !== 'undefined' && '__REDUX_DEVTOOLS_EXTENSION_COMPOSE__' in window) {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
  }

  const loggerMiddleware = createLogger();
  middlewares.push(loggerMiddleware);
}

export default (state = {}) => {
  const store = createStore(
    rootReducer,
    state,
    composeEnhancers(
      applyMiddleware(...middlewares)
    )
  );

  sagaMiddleware.run(rootSaga);
  store.runSaga = () => sagaMiddleware.run(rootSaga);
  store.close = () => store.dispatch(END);

  return store;
};
