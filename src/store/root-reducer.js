import {combineReducers} from 'redux';

import {
  filmList,
  searchData,
  singleFilm,
  filmsSaga,
  filmsInfoSaga,
  similarFilmsSaga,
} from 'store/';

import {all} from 'redux-saga/effects';

function* rootSaga() {
  yield all([
    filmsSaga(),
    filmsInfoSaga(),
    similarFilmsSaga(),
  ]);
}

const rootReducer = combineReducers({
  filmList,
  searchData,
  singleFilm,
});

export {
  rootReducer,
  rootSaga,
};

