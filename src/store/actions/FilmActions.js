import {
  FETCH_FILMS_REQUEST,
  FETCH_FILMS_SUCCESS,
  FETCH_FILMS_FAILURE,
  SET_SEARCH,
  CLEAR_FILMS,
  API_ROOT,
} from 'constants/';

import {addParamsToUrl} from 'helpers/';

import {call, put, all, takeLatest, select} from 'redux-saga/effects';

export const fetchFilmsRequest = () => ({type: FETCH_FILMS_REQUEST});
export const fetchFilmsFailure = () => ({type: FETCH_FILMS_FAILURE});
export const clearFilmsList = () => ({type: CLEAR_FILMS});

export const fetchFilmsSuccess = (json) => ({
  type: FETCH_FILMS_SUCCESS,
  films: json.data,
});

// Sagas
export function* loadSearchPageAsync() {
  try {
    const searchData = yield select((state) => state.searchData);

    yield put(fetchFilmsRequest());

    const response = yield call(fetch, addParamsToUrl(`${API_ROOT}`, searchData));
    const data = yield response.json();

    yield put(fetchFilmsSuccess(data));
  } catch (e) {
    yield put(fetchFilmsFailure());
  }
}

export function* watchSetSearchDataAction() {
  yield takeLatest(SET_SEARCH, loadSearchPageAsync);
}
// films Saga
export function* filmsSaga() {
  yield all([
    watchSetSearchDataAction(),
  ]);
}