import {
  FETCH_FILM_INFO_REQUEST,
  FETCH_FILM_INFO_SUCCESS,
  FETCH_FILM_INFO_FAILURE,
  FETCH_FILM_INFO,
  CLEAR_FILM_INFO,
  FETCH_SIMILAR_FILMS,
  API_ROOT,
} from 'constants/';

import {
  fetchFilmsSuccess,
} from 'store/';

import {call, put, all, takeLatest, select} from 'redux-saga/effects';

export const fetchFilmInfoRequest = () => ({type: FETCH_FILM_INFO_REQUEST});
export const fetchFilmInfoFailure = () => ({type: FETCH_FILM_INFO_FAILURE});
export const clearFilmInfo = () => ({type: CLEAR_FILM_INFO});

export const fetchFilmInfoSuccess = (json) => ({
  type: FETCH_FILM_INFO_SUCCESS,
  singleFilm: json,
});

export const fetchFilmInfo = (id) => ({
  type: FETCH_FILM_INFO,
  payload: id,
});

// Sagas
export function* fetchFilmInfoAsync(action) {
  try {
    yield put(fetchFilmInfoRequest());

    const response = yield call(fetch, API_ROOT + action.payload);
    const data = yield response.json();

    yield put(fetchFilmInfoSuccess(data));

    yield call(fetchSimilarFilmsAsync);
  } catch (e) {
    yield put(fetchFilmInfoFailure());
  }
}

export function* watchFetchFilmInfo() {
  yield takeLatest(FETCH_FILM_INFO, fetchFilmInfoAsync);
}

// filmsInfo Saga
export function* filmsInfoSaga() {
  yield all([
    watchFetchFilmInfo(),
  ]);
}

// export const fetchFilmsSuccess = (json) => ({
//   type: FETCH_FILMS_SUCCESS,
//   films: json.data,
// });
//
export const fetchSimilarFilms = () => ({
  type: FETCH_SIMILAR_FILMS,
});

// Sagas
export function* fetchSimilarFilmsAsync() {
  try {
    const filmInfo = yield select((state) => state.singleFilm);

    const genre = filmInfo.genres ? filmInfo.genres[0] : '';
    const searchQuery = `?search=${genre}&sortBy=vote_average&sortOrder=desc&searchBy=genres&limit=9`;
    const response = yield call(fetch, API_ROOT + searchQuery);
    const data = yield response.json();

    yield put(fetchFilmsSuccess(data));
  } catch (e) {
    console.error(e);
  }
}

export function* watchFetchSimilarFilms() {
  yield takeLatest(FETCH_SIMILAR_FILMS, fetchSimilarFilmsAsync);
}

// filmsInfo Saga
export function* similarFilmsSaga() {
  yield all([
    watchFetchSimilarFilms(),
  ]);
}