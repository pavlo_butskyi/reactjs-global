import {
  SET_SEARCH,
  CLEAR_SEARCH,
} from 'constants/';

export const setSearchData = (searchData) => ({
  type: SET_SEARCH,
  searchData,
});

export const clearSearchData = () => ({
  type: CLEAR_SEARCH,
});