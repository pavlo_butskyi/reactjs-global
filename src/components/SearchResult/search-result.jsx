import React from 'react';
import PropTypes from 'prop-types';
import {
  FilmList,
  EmptyResult,
} from 'components';

export class SearchResult extends React.Component {
  static get propTypes() {
    return {
      filmList: PropTypes.object,
    };
  }

  render() {
    const films = this.props.filmList.films || [];

    if (this.props.filmList.isFetching) {
      {return <EmptyResult text="Loading..." />;}
    }

    if (films.length) {
      {return <FilmList films={films} />;}
    } else {
      {return <EmptyResult text="No films found" />;}
    }
  }
}