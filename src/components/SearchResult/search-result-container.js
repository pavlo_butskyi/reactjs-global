import {
  SearchResult as SearchResultClass,
} from './search-result';

import {
  selectFilmList,
} from 'store/';

import {connect} from 'react-redux';

function mapStateToProps(state) {
  return {
    filmList: selectFilmList(state),
  };
}

export const SearchResult = connect(mapStateToProps)(SearchResultClass);