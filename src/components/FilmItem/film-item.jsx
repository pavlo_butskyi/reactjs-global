import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import './style.scss';

export class FilmItem extends React.Component {
  static get propTypes() {
    return {
      id: PropTypes.number,
      genres: PropTypes.array,
      release_date: PropTypes.string,
      poster_path: PropTypes.string,
      title: PropTypes.string,
    };
  }

  render() {
    const genres = this.props.genres.join(', ') || '';
    return (
      <div className="film-item">
        <Link to={`/film/${this.props.id}`}>
          <img
            src={this.props.poster_path}
            alt={this.props.title}
            className="film-item_img"
          />
          <div className="film-item_header">
            <span className="film-item_name">
              {this.props.title}
            </span>
            <span className="film-item_year">
              {(new Date(this.props.release_date)).getFullYear()}
            </span>
            <span className="film-item_genre">
              {genres}
            </span>
          </div>
        </Link>
      </div>
    );
  }
}
