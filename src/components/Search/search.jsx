import React from 'react';
import PropTypes from 'prop-types';
import {
  Radio,
} from 'components';

import './style.scss';

import {addParamsToUrl} from 'helpers/';

export class Search extends React.Component {
  static get propTypes() {
    return {
      searchData: PropTypes.object,
      history: PropTypes.object,
      // searchActions: PropTypes.object,
      setSearchData: PropTypes.func,
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      search: props.searchData.search,
      searchBy: props.searchData.searchBy,
    };

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleFormSubmit(e) {
    e.preventDefault();
    this.props.setSearchData(this.state);

    // TODO: Will find a solution to handle this with async/await function
    // Race condition when we submit form from Home page.
    setTimeout(() => {
      this.props.history.push(addParamsToUrl('/search/', this.props.searchData));
    },10);
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    this.setState({
      search: newProps.searchData.search,
      searchBy: newProps.searchData.searchBy,
    });
  }

  render() {
    return (
      <form
        className="search"
        onSubmit={this.handleFormSubmit}
      >
        <p className="search_title">
          {'Find your movie'}
        </p>
        <input
          type='text'
          className='search_input'
          placeholder='search...'
          name='search'
          onChange={this.handleChange}
          value={this.state.search}
        />

        <div className="search_footer">
          <div className="search_type">
            <p className="search_type-title">
              {'search by'}
            </p>
            <div className="search_type-btn">
              <Radio
                name='searchBy'
                label='Title'
                value='title'
                onChange={this.handleChange}
                isChecked={this.state.searchBy === 'title'}
              />
            </div>
            <div className="search_type-btn">
              <Radio
                name='searchBy'
                label='Genre'
                value='genres'
                onChange={this.handleChange}
                isChecked={this.state.searchBy === 'genres'}
              />
            </div>
          </div>

          <button
            className='search_submit'
            type='submit'
          >
            {'Search'}
          </button>
        </div>
      </form>
    );
  }
}