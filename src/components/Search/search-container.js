import {
  Search as SearchFormClass,
} from './search';

import {
  withRouter,
} from 'react-router';

import {
  bindActionCreators,
} from 'redux';

import {
  connect,
} from 'react-redux';

import {
  setSearchData,
  selectSearchData,
} from 'store/';

function mapStateToProps(state) {
  return {
    searchData: selectSearchData(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setSearchData: bindActionCreators(setSearchData, dispatch),
  };
}

export const Search = withRouter(connect(mapStateToProps, mapDispatchToProps)(SearchFormClass));