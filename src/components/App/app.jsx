import React from 'react';
import PropTypes from 'prop-types';

export class App extends React.Component {
  static get propTypes() {
    return {
      children: PropTypes.node,
    };
  }

  render() {
    return (
      <div className='app'>
        {this.props.children}
      </div>
    );
  }
}