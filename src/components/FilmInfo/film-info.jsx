import React from 'react';
import PropTypes from 'prop-types';
import {
  Row,
  Col,
} from 'reactstrap';
import './style.scss';

export class FilmInfo extends React.Component {
  static get propTypes() {
    return {
      id: PropTypes.number,
      genres: PropTypes.array,
      release_date: PropTypes.string,
      runtime: PropTypes.number,
      poster_path: PropTypes.string,
      title: PropTypes.string,
      vote_average: PropTypes.number,
      overview: PropTypes.string,
    };
  }

  render() {
    if (this.props.id) {
      const genres = this.props.genres.join(', ') || '';
      const year = (new Date(this.props.release_date)).getFullYear();
      const runtime = `${this.props.runtime} min`;
      return (
        <div className="film-info">
          <Row>
            <Col sm="4">
              <img
                src={this.props.poster_path}
                alt={this.props.title}
                className="film-info_poster"
              />
            </Col>
            <Col sm="8">
              <div className="film-info_details">
                <h1 className="film-info_name">
                  {this.props.title}
                  <span className="film-info_rating">{this.props.vote_average}</span>
                </h1>
                <p className="film-info_genre">
                  {genres}
                </p>
                <p className="film-info_info">
                  <span>
                    {year}
                  </span>
                  <span>
                    {runtime}
                  </span>
                </p>
                <p className="film-info_desc">
                  {this.props.overview}
                </p>
              </div>
            </Col>
          </Row>
        </div>
      );
    } else {
      return (
        <h2 className="film-info_empty">
          {'Loading...'}
        </h2>
      );
    }
  }
}