import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

export class Header extends React.Component {
  static get propTypes() {
    return {
      children: PropTypes.node,
    };
  }

  render() {
    return (
      <header className="header">
        {this.props.children}
      </header>
    );
  }
}