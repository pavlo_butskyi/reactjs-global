import React from 'react';
import PropTypes from 'prop-types';

export class SameGenreList extends React.Component {
  static get propTypes() {
    return {
      genres: PropTypes.string,
    };
  }

  render() {
    return (
      <span>
        {'Films by'}
        <strong>
          {this.props.genres}
        </strong>
        {'genres'}
      </span>
    );
  }
}