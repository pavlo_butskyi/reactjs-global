import React from 'react';
import PropTypes from 'prop-types';
import {
  Container,
  Row,
  Col,
} from 'reactstrap';
import './style.scss';

export class SubHeader extends React.Component {
  static get propTypes() {
    return {
      children: PropTypes.node,
    };
  }

  render() {
    return (
      <div className='sub-header'>
        <Container>
          <Row>
            <Col>
              <div className='sub-header_inner'>
                {this.props.children}
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}