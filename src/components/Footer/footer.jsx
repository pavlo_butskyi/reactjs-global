import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

export class Footer extends React.Component {
  static get propTypes() {
    return {
      children: PropTypes.node,
    };
  }

  render() {
    return (
      <footer className="footer">
        {this.props.children}
      </footer>
    );
  }
}