import React from 'react';
import PropTypes from 'prop-types';
import {
  FilmItem,
  EmptyResult,
} from 'components';

import './style.scss';

export class FilmList extends React.Component {
  static get propTypes() {
    return {
      films: PropTypes.array,
    };
  }

  render() {
    if (this.props.films) {
      return (
        <div className="film-list">
          {
            this.props.films.map((item, index) => (
              <FilmItem
                key={index}
                {...item}
              />)
            )
          }
        </div>
      );
    } else {
      return <EmptyResult text="No Films Found" />;
    }
  }
}