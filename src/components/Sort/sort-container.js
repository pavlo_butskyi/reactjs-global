import {
  Sort as SortClass,
} from './sort';
import './style.scss';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {
  setSearchData,
  selectSearchData,
} from 'store/';
import {withRouter} from 'react-router';

function mapStateToProps(state) {
  return {
    searchData: state.searchData,
    sortBy: selectSearchData(state).sortBy,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setSearchData: bindActionCreators(setSearchData, dispatch),
  };
}

export const Sort = withRouter(connect(mapStateToProps, mapDispatchToProps)(SortClass));