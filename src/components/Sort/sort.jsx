import React from 'react';
import PropTypes from 'prop-types';

import {
  Radio,
} from 'components';

import './style.scss';

import {addParamsToUrl} from 'helpers/';

export class Sort extends React.Component {
  static get propTypes() {
    return {
      setSearchData: PropTypes.func,
      searchData: PropTypes.object,
      history: PropTypes.object,
      sortBy: PropTypes.string,
    };
  }

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.props.setSearchData({sortBy: e.target.value});

    // redirect to searchPage with search data
    this.props.history.push(addParamsToUrl('/search/', this.props.searchData));
  }

  render() {
    return (
      <form className='sort'>
        <div className='sort_title'>
          {'Sort by'}
        </div>
        <div className='sort_btn'>
          <Radio
            name='sortBy'
            label='Release date'
            value='release_date'
            mod='sort'
            onChange={this.handleChange}
            isChecked={this.props.sortBy === 'release_date'}
          />
        </div>
        <div className='sort_btn'>
          <Radio
            name='sortBy'
            label='Rating'
            value='vote_average'
            mod='sort'
            onChange={this.handleChange}
            isChecked={this.props.sortBy === 'vote_average'}
          />
        </div>
      </form>
    );
  }
}