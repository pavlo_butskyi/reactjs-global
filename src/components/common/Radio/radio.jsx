import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

export class Radio extends React.Component {
  static get propTypes() {
    return {
      mod: PropTypes.string,
      name: PropTypes.string.isRequired,
      isChecked: PropTypes.bool,
      onChange: PropTypes.func,
      value: PropTypes.any,
      label: PropTypes.string,
    };
  }

  render() {
    let className = 'radio';
    if (this.props.mod) {
      className += ` radio--${this.props.mod}`;
    }

    return (
      <label className={className}>
        <input
          type='radio'
          name={this.props.name}
          checked={this.props.isChecked}
          onChange={this.props.onChange}
          value={this.props.value}
        />
        <span>
          {this.props.label}
        </span>
      </label>
    );
  }
}