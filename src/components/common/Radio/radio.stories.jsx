import React from 'react';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {
  Radio,
} from './radio';

storiesOf('Radio', module)
  .add('checked', () => (
    <Radio
      name='name'
      label='label'
      value='value'
      onChange={action('clicked')}
      isChecked={!0}
    />
  ))
  .add('unchecked', () => (
    <Radio
      name='name'
      label='label'
      value='value'
      onChange={action('clicked')}
      isChecked={!1}
    />
  ));