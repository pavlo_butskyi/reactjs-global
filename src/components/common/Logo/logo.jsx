import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

export class Logo extends React.Component {
  static get propTypes() {
    return {
      siteName: PropTypes.string,
    };
  }

  render() {
    return (
      <div className="logo">
        {this.props.siteName}
      </div>
    );
  }
}