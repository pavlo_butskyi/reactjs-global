import React from 'react';
import {storiesOf} from '@storybook/react';
import {
  EmptyResult,
} from './empty-result';

storiesOf('Empty Result', module)
  .add('with custom text', () => (
    <EmptyResult text="Custom message text" />
  ));