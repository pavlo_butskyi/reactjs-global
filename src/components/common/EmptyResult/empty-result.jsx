import React from 'react';

import PropTypes from 'prop-types';
import {
  Container,
  Row,
  Col,
} from 'reactstrap';
import './style.scss';

export class EmptyResult extends React.Component {
  static get propTypes() {
    return {
      text: PropTypes.string,
    };
  }

  render() {
    return (
      <Container>
        <Row>
          <Col>
            <div className="empty-result">
              {this.props.text}
            </div>
          </Col>
        </Row>
      </Container>
    );
  }
}