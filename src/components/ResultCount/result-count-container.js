import {
  ResultCount as ResultCountClass,
} from './result-count';

import {
  selectFilmsCount,
} from 'store/';

import {connect} from 'react-redux';

const mapStateToProps = (state) => ({

  filmsCount: selectFilmsCount(state),
});

export const ResultCount = connect(mapStateToProps)(ResultCountClass);