import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

export class ResultCount extends React.Component {
  static get propTypes() {
    return {
      filmsCount: PropTypes.number,
    };
  }

  render() {
    const filmCount = this.props.filmsCount;

    if (!filmCount) {
      return (
        <div className="result-count">
          {'No movies found'}
        </div>
      );
    }

    const moviesFound = `${filmCount} movies found`;
    return (
      <div className="result-count">
        {moviesFound}
      </div>
    );
  }
}