import React from 'react';
import PropTypes from 'prop-types';

import {
  Container,
  Row,
  Col,
} from 'reactstrap';

import {
  Header,
  Logo,
  Search,
  SubHeader,
  ResultCount,
  Sort,
  Footer,
  SearchResult,
} from 'components';

import {urlToObject, validateUrlData} from 'helpers/';

export class SearchPage extends React.Component {
  static get propTypes() {
    return {
      searchData: PropTypes.object,
      location: PropTypes.object,
      setSearchData: PropTypes.func,
      clearSearchData: PropTypes.func,
      clearFilmsList: PropTypes.func,
    };
  }

  constructor(props) {
    super(props);

    this.setSearchDataFromURL();
  }

  setSearchDataFromURL() {
    const allowedParams = ['search', 'searchBy', 'sortBy', 'sortOrder', 'limit'];

    const searchQuery = this.props.location.search;

    // search query object with allowed params only
    const searchData = searchQuery
      ? validateUrlData(urlToObject(searchQuery.substring(1)), allowedParams)
      : this.props.searchData;

    this.props.setSearchData(searchData);
  }

  componentWillUnmount() {
    this.props.clearSearchData();
    this.props.clearFilmsList();
  }

  render() {
    return (
      <React.Fragment>
        <Header>
          <Container>
            <Row>
              <Col>
                <Logo siteName="netflixroulete" />
              </Col>
            </Row>
            <Row>
              <Col>
                <Search />
              </Col>
            </Row>
          </Container>
        </Header>
        <SubHeader>
          <ResultCount />
          <Sort />
        </SubHeader>
        <main>
          <Container>
            <Row>
              <Col>
                <SearchResult />
              </Col>
            </Row>
          </Container>
        </main>
        <Footer>
          <Container>
            <Row>
              <Col>
                <Logo siteName="netflixroulete" />
              </Col>
            </Row>
          </Container>
        </Footer>
      </React.Fragment>
    );
  }
}