import {
  SearchPage as SearchPageClass,
} from './search-page';

import {
  setSearchData,
  clearSearchData,
  clearFilmsList,
  selectSearchData,
} from 'store/';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

function mapStateToProps(state) {
  return {
    searchData: selectSearchData(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setSearchData: bindActionCreators(setSearchData, dispatch),
    clearSearchData: bindActionCreators(clearSearchData, dispatch),
    clearFilmsList: bindActionCreators(clearFilmsList, dispatch),
  };
}

export const SearchPage = connect(mapStateToProps, mapDispatchToProps)(SearchPageClass);
