import React from 'react';

import {
  Container,
  Row,
  Col,
} from 'reactstrap';

import {
  Header,
  Logo,
  Search,
  Footer,
  EmptyResult,
} from 'components';

export class HomePage extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Header>
          <Container>
            <Row>
              <Col>
                <Logo siteName="netflixroulete" />
              </Col>
            </Row>
            <Row>
              <Col>
                <Search />
              </Col>
            </Row>
          </Container>
        </Header>
        <main>
          <Container>
            <Row>
              <Col>
                <EmptyResult text="Use search form to find films" />
              </Col>
            </Row>
          </Container>
        </main>
        <Footer>
          <Container>
            <Row>
              <Col>
                <Logo siteName="netflixroulete" />
              </Col>
            </Row>
          </Container>
        </Footer>
      </React.Fragment>
    );
  }
}
