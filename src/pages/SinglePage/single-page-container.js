import {
  SinglePage as SinglePageClass,
} from './single-page';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {
  fetchFilmInfo,
  clearFilmInfo,
  clearFilmsList,
  selectFilmInfo,
} from 'store/';

function mapStateToProps(state) {
  return {
    filmInfo: selectFilmInfo(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchFilmInfo: bindActionCreators(fetchFilmInfo, dispatch),
    clearFilmInfo: bindActionCreators(clearFilmInfo, dispatch),
    clearFilmsList: bindActionCreators(clearFilmsList, dispatch),
  };
}

export const SinglePage = connect(mapStateToProps, mapDispatchToProps)(SinglePageClass);