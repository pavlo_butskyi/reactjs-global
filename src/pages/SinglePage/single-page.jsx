import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {
  Container,
  Row,
  Col,
} from 'reactstrap';

import {Link} from 'react-router-dom';

import {
  Header,
  Logo,
  FilmInfo,
  SubHeader,
  SameGenreList,
  Footer,
  SearchResult,
  EmptyResult,
} from 'components';

export class SinglePage extends Component {
  static get propTypes() {
    return {
      filmInfo: PropTypes.object,
      location: PropTypes.object,
      match: PropTypes.object,

      fetchFilmInfo: PropTypes.func,
      clearFilmInfo: PropTypes.func,
      clearFilmsList: PropTypes.func,
    };
  }

  constructor(props) {
    super(props);
    props.fetchFilmInfo(this.props.match.params.movieId);
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.props.fetchFilmInfo(this.props.match.params.movieId);
    }
  }

  componentWillUnmount() {
    this.props.clearFilmInfo();
    this.props.clearFilmsList();
  }

  render() {
    const singleGenres = this.props.filmInfo.genres ? this.props.filmInfo.genres[0] : '';

    let FilmInfoRender;
    if (this.props.filmInfo.isError) {
      FilmInfoRender = <EmptyResult text='Film does not exist' />;
    } else if (this.props.filmInfo.isFetching) {
      FilmInfoRender = <EmptyResult text='Loading...' />;
    } else {
      FilmInfoRender = <FilmInfo {...this.props.filmInfo} />;
    }
    return (
      <React.Fragment>
        <Header>
          <Container>
            <Row>
              <Col>
                <div className='header_top'>
                  <Logo siteName='netflixroulete' />
                  <Link
                    className='header_btn'
                    to='/'
                  >
                    {'Search'}
                  </Link>
                </div>
              </Col>
            </Row>
            <Row>
              <Col>
                {FilmInfoRender}
              </Col>
            </Row>
          </Container>
        </Header>

        <SubHeader>
          {!this.props.filmInfo.isError && <SameGenreList genres={singleGenres} />}
        </SubHeader>

        <main>
          <Container>
            <Row>
              <Col>
                {!this.props.filmInfo.isError && <SearchResult />}
              </Col>
            </Row>
          </Container>
        </main>

        <Footer>
          <Container>
            <Row>
              <Col>
                <Logo siteName="netflixroulete" />
              </Col>
            </Row>
          </Container>
        </Footer>
      </React.Fragment>
    );
  }
}