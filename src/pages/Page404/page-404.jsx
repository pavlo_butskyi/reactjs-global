import React from 'react';
import {
  Container,
  Row,
  Col,
} from 'reactstrap';

import {
  Header,
  Logo,
  Footer,
  EmptyResult,
} from 'components';

import {
  Link,
} from 'react-router-dom';

export class Page404 extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Header>
          <Container>
            <Row>
              <Col>
                <div className='header_top'>
                  <Logo siteName='netflixroulete' />
                  <Link
                    className='header_btn'
                    to='/'
                  >
                    {'Search'}
                  </Link>
                </div>
              </Col>
            </Row>
          </Container>
        </Header>
        <main>
          <Container>
            <Row>
              <Col>
                <EmptyResult text='Page not found' />
              </Col>
            </Row>
          </Container>
        </main>
        <Footer>
          <Container>
            <Row>
              <Col>
                <Logo siteName="netflixroulete" />
              </Col>
            </Row>
          </Container>
        </Footer>
      </React.Fragment>
    );
  }
}
