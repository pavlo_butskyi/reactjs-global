export * from './add-params-to-url';

export const validateUrlData = (raw = {}, allowed = []) => (
  Object.keys(raw)
    .filter((key) => allowed.includes(key))
    .reduce((obj, key) => ({
      ...obj,
      [key]: raw[key],
    }), {})
);

export const urlToObject = (string) => {
  let store = {};
  try {
    store = JSON.parse(`{"${decodeURI(string).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"')}"}`);
  } catch (e) {
    console.error(e);
  }
  return store;
};

