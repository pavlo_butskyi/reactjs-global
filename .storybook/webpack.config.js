const path = require('path');

const genDefaultConfig = require('@storybook/react/dist/server/config/defaults/webpack.config.js');

module.exports = (baseConfig, env) => {
  const config = genDefaultConfig(baseConfig, env);

  config.module.rules.push({
    test: /\.scss$/,
    include: path.resolve(__dirname, '../src'),
    use: [
      'style-loader',
      'css-loader',
      {
        loader: 'sass-loader',
        options: {
          data: '@import "variables";',
          includePaths: [
            path.resolve(__dirname, '../src/styles')
          ]
        }
      }]
  });

  return config;
};
